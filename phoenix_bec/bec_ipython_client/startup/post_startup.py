"""
Post startup script for the BEC client. This script is executed after the
IPython shell is started. It is used to load the beamline specific
information and to setup the prompts.

The script is executed in the global namespace of the IPython shell. This
means that all variables defined here are available in the shell.

While command-line arguments have to be set in the pre-startup script, the
post-startup script can be used to load beamline specific information and
to setup the prompts.

    from bec_lib.logger import bec_logger

    logger = bec_logger.logger

    # pylint: disable=import-error
    _args = _main_dict["args"]

    _session_name = "cSAXS"
    if _args.session.lower() == "lamni":
        from csaxs_bec.bec_ipython_client.plugins.cSAXS import *
        from csaxs_bec.bec_ipython_client.plugins.LamNI import *

        _session_name = "LamNI"
        lamni = LamNI(bec)
        logger.success("LamNI session loaded.")

    elif _args.session.lower() == "csaxs":
        print("Loading cSAXS session")
        from csaxs_bec.bec_ipython_client.plugins.cSAXS import *

        logger.success("cSAXS session loaded.")
"""

# pylint: disable=invalid-name, unused-import, import-error, undefined-variable, unused-variable, unused-argument, no-name-in-module
import time as tt
import sys
import os
from IPython.core.magic import register_line_magic


from bec_lib.logger import bec_logger

logger = bec_logger.logger
# logger = bec_logger.LOGLEVEL.TRACE

# pylint: disable=invald-name, unused-import, import-error, undefined-variable, unused-variable, unused-argument, no-name-in-module

_session_name = "session_phoenix"
# SETUP PROMPTS
bec._ip.prompts.username = "PHOENIX"
bec._ip.prompts.status = 1

# make sure that edited modules are reloaded when changed

print("phoenix_bec/bec_iphyon_client/startup/post_startup.py")
print("... set autoreload of modules")
bec._ip.run_line_magic("reload_ext", "autoreload")
bec._ip.run_line_magic("autoreload", "2")

print("autoreload loaded ")


#############################################################################
#
# ... register BL specific magic commands
#
##############################################################################


@register_line_magic
def ph_reload(line):

    ##########################################################################
    #
    # this reloads certain phoenix related script to the iphython shell.
    # useful for debugging/development to be revised for production
    # aim of this magic is to quickl reload BL related stuff for debugging
    # Most likely there are better ways to do this (possibly bec.reload_user_script()
    # but syntax not clear, error messages. Here we know what we do
    #
    ###################################################################W#####

    from phoenix_bec.scripts import phoenix as PH

    print("reload phoenix_bec.scripts.phoenix to iphyhton console")
    print("to update version server restart server ")
    # need to use global statement here, as I like to reload into space on
    # iphyton consoel
    global PH, phoenix
    print("from phoenix_bec.scripts import phoenix as PH")
    print("phoenix = PH.PhoenixBL()")
    phoenix = PH.PhoenixBL()

    # ph_config=PH.PhoenixConfighelper()


# enddef

print("##################################################################")
print("register magic")
print("...... %ph_load_xmap      ...   to reload xmap configuration")


@register_line_magic
def ph_add_xmap(line):
    """
    magic for loading xmap
    """
    t0 = tt.time()
    phoenix.add_xmap()
    print("elapsed time:", tt.time() - t0)


# enddef

print("...... %ph_load_falcon   ...   to reload falcon configuration")


@register_line_magic
def ph_add_falcon(line):
    """
    magic to add  falcon to existing configuration
    """
    t0 = tt.time()
    phoenix.add_falcon()
    print("elapsed time:", tt.time() - t0)


## enddef


@register_line_magic
def ph_load_falcon(line):
    """
    magic to load falcon as sole detector
    """
    t0 = tt.time()
    phoenix.load_falcon()
    print("elapsed time:", tt.time() - t0)


print("...... %ph_load_config   ...   to reload phoenix default configuration")


@register_line_magic
def ph_create_base_config(line):
    """
    load base configuration for PHOENIX beamline
    using phoenix.create_base_config()
    phoenix_bec/device_configs/phoenix_devices.yaml
    """
    t0 = tt.time()
    phoenix.create_base_config()
    print("elapsed time:", tt.time() - t0)


### enddef


@register_line_magic
def ph_restart_bec_server(line):
    os.system("bec-server restart")
    os.system(
        'gnome-terminal --geometry 170X50 -- bash -c "source /data/test/x07mb-test-bec/bec_deployment/bec_venv/bin/activate ;  bec-server attach; exec bash"'
    )


#    #import phoenix_bec.bec_ipython_client.startup.post_startup
#    does not work seems to build a infinite stack...

####################################################################################
#
#  init phoenix.py from server version as
# .................. phoenix_server=PhoenixBL()
#  and in ipython shell only as
#  .............. phoenix = Ph.Phoenix(BL()
##
#####################################################################################

print("###############################################################")
print("init phoenix_bec/scripts/phoenix.py in two different ways")
print("    1) phoenix_server = PhoenixBL()  ... takes code from server version ")
print("SERVR VERSION DOES NOT WORK ANYMORE ")
print("FOLDER SCRIPT SEEMS TO BE NON_STANDARD!!!!!!! ")

phoenix_server = PhoenixBL()

print("    2) phoenix=PH.PhoenixBL()  ... on inpython shell only!  (for debugging)")

from phoenix_bec.scripts import phoenix as PH

phoenix = PH.PhoenixBL()


# from phoenix_bec.bec_ipython_client.plugins.phoenix import Phoenix
# from phoenix_bec.devices.falcon_phoenix_no_hdf5 import FalconHDF5Plugins
