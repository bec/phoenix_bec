"""
Implementation for falcon at PHOENIX, derived from
implementation on csaxs (file falcon_csaxs.py)

18.10.2024 further development of falcon_phoenix.y to phoenix to sitoro_phoenix.py
Now we use the definition of all EPICS channels for falcon as defined in the classes in sitoro.py

WIP......

17.10.2024 try to streamline implementation with mca record


Differences to implement

1) we consider EPICS initialization as standard implementaion,
    so no reinitialization when bec device is initrialized  ... DONE ...

2) in EpicsDXPFalcon(Device) add ICR and OCR for individual detectors

3) can we make this generic to make it suited for both falcon and XMAP ?

3) make easy switching between mca spectra an mca mapping

fix defiend relation bwetween variables and names used here for example DONE

    aquiring is currently called 'state'   --> should be renamed to aquiring
    Currently   state = Cpt(EpicsSignal, "Acquiring")
    should be   acquiring =  Cpt(EpicsSignal, "Acquiring")


    hdf5 = Cpt(FalconHDF5Plugins, "HDF1:")

def arm_aquisition

      raise FalconTimeoutError(
                f"Failed to arm the acquisition. Detector state {signal_conditions[0][0]}"
            )



CHANGES LOG and

    System as taken from cSAXS some times works for one element need about 7 second
    There seem to be still serious timout issues

    changes log
    TIMEOUT_FOR_SIGNALs  from 5 to 10




"""

import enum
import threading
import time

from bec_lib.logger import bec_logger

from ophyd import Component as Cpt
from ophyd import Device, EpicsSignal, EpicsSignalRO, EpicsSignalWithRBV

# from ophyd.mca import EpicsMCARecord  # old import
# now import ophyd.mca completely

# import ophyd.mca as Mca

from .sitoro import (
    SitoroEpicsMCARecord,
    SitoroEpicsMCA,
    SitoroEpicsMCAReadNotify,
    SitoroEpicsDXP,
    SitoroEpicsDXPBaseSystem,
    SitoroEpicsDXPMultiElementSystem,
    SitoroTest,
)

from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin,
    PSIDetectorBase,
)

logger = bec_logger.logger


class SitoroError(Exception):
    """Base class for exceptions in this module."""


class SitoroTimeoutError(SitoroError):
    """Raised when the Sitoro does not respond in time."""


class DetectorState(enum.IntEnum):
    """Detector states for Sitoro detector"""

    DONE = 0
    ACQUIRING = 1


class TriggerSource(enum.IntEnum):
    """Trigger source for Sitoro detector"""

    USER = 0
    GATE = 1
    SYNC = 2


class MappingSource(enum.IntEnum):
    """Mapping source for Sitoro detector"""

    SPECTRUM = 0
    MAPPING = 1


class SitoroEpicsMCARecordExtended_OLD(SitoroEpicsMCARecord):

    # add parameters for detector energy calibration
    # which are missing in mca.py

    calo = Cpt(EpicsSignal, ".CALO")
    cals = Cpt(EpicsSignal, ".CALS")
    calq = Cpt(EpicsSignal, ".CALQ")
    tth = Cpt(EpicsSignal, ".TTH")


class SitoroEpicsDXP_OLD(Device):
    """
    DXP parameters for Sitoro detector

    Base class to map EPICS PVs from DXP parameters to ophyd signals.
    """

    elapsed_live_time = Cpt(EpicsSignal, "ElapsedLiveTime")
    elapsed_real_time = Cpt(EpicsSignal, "ElapsedRealTime")
    elapsed_trigger_live_time = Cpt(EpicsSignal, "ElapsedTriggerLiveTime")

    # Energy Filter PVs
    energy_threshold = Cpt(EpicsSignalWithRBV, "DetectionThreshold")
    min_pulse_separation = Cpt(EpicsSignalWithRBV, "MinPulsePairSeparation")
    detection_filter = Cpt(EpicsSignalWithRBV, "DetectionFilter", string=True)
    scale_factor = Cpt(EpicsSignalWithRBV, "ScaleFactor")
    risetime_optimisation = Cpt(EpicsSignalWithRBV, "RisetimeOptimization")
    decay_time = Cpt(EpicsSignalWithRBV, "DecayTime")

    current_pixel = Cpt(EpicsSignalRO, "CurrentPixel")


class SitoroHDF5Plugins(Device):
    """
    HDF5 parameters for Sitoro detector

    Base class to map EPICS PVs from HDF5 Plugin to ophyd signals.
    """

    capture = Cpt(EpicsSignalWithRBV, "Capture")
    enable = Cpt(EpicsSignalWithRBV, "EnableCallbacks", string=True, kind="config")
    xml_file_name = Cpt(EpicsSignalWithRBV, "XMLFileName", string=True, kind="config")
    lazy_open = Cpt(EpicsSignalWithRBV, "LazyOpen", string=True, doc="0='No' 1='Yes'")
    temp_suffix = Cpt(EpicsSignalWithRBV, "TempSuffix", string=True)
    file_path = Cpt(EpicsSignalWithRBV, "FilePath", string=True, kind="config")
    file_name = Cpt(EpicsSignalWithRBV, "FileName", string=True, kind="config")
    file_template = Cpt(EpicsSignalWithRBV, "FileTemplate", string=True, kind="config")
    num_capture = Cpt(EpicsSignalWithRBV, "NumCapture", kind="config")
    file_write_mode = Cpt(EpicsSignalWithRBV, "FileWriteMode", kind="config")
    queue_size = Cpt(EpicsSignalWithRBV, "QueueSize", kind="config")
    array_counter = Cpt(EpicsSignalWithRBV, "ArrayCounter", kind="config")


class SitoroSetup(CustomDetectorMixin):
    """
    Sitoro setup class for Phoenix

    Parent class: CustomDetectorMixin

    """

    def __init__(self, *args, parent: Device = None, **kwargs) -> None:
        super().__init__(*args, parent=parent, **kwargs)
        self._lock = threading.RLock()

    def on_init(self) -> None:
        """Initialize Sitoro detector"""
        self.initialize_default_parameter()
        self.initialize_detector()
        self.initialize_detector_backend()

    def initialize_default_parameter(self) -> None:
        """
        Set default parameters for Sitoro

        This will set:
        - readout (float): readout time in seconds
        - value_pixel_per_buffer (int): number of spectra in buffer of Sitoro Sitoro

        """
        # self.parent.value_pixel_per_buffer = 20
        self.update_readout_time()

    def update_readout_time(self) -> None:
        """Set readout time for Eiger9M detector"""
        readout_time = (
            self.parent.scaninfo.readout_time
            if hasattr(self.parent.scaninfo, "readout_time")
            else self.parent.MIN_READOUT
        )
        self.parent.readout_time = max(readout_time, self.parent.MIN_READOUT)

    def initialize_detector(self) -> None:
        """Initialize Sitoro detector"""

        pass

        """
        THIS IS THE OLD CSACS CODE. uncomment for now, as we consider EPICS as the boss
        for initialization

        self.stop_detector()
        self.stop_detector_backend()

        self.set_trigger(
            mapping_mode=MappingSource.MAPPING, trigger_source=TriggerSource.GATE, ignore_gate=0
        )
        # 1 Realtime
        self.parent.preset_mode.put(1)
        # 0 Normal, 1 Inverted
        self.parent.input_logic_polarity.put(0)
        # 0 Manual 1 Auto
        self.parent.auto_pixels_per_buffer.put(0)
        # Sets the number of pixels/spectra in the buffer
        self.parent.pixels_per_buffer.put(self.parent.value_pixel_per_buffer)
        """

    def initialize_detector_backend(self) -> None:
        """Initialize the detector backend for Sitoro."""
        self.parent.hdf5.enable.put(1)
        # file location of h5 layout for cSAXS
        self.parent.hdf5.xml_file_name.put("layout.xml")
        # TODO Check if lazy open is needed and wanted!
        self.parent.hdf5.lazy_open.put(1)
        self.parent.hdf5.temp_suffix.put("")
        # size of queue for number of spectra allowed in the buffer, if too small at high throughput, data is lost
        self.parent.hdf5.queue_size.put(2000)
        # Segmentation into Spectra within EPICS, 1 is activate, 0 is deactivate
        self.parent.nd_array_mode.put(1)

    def on_stage(self) -> None:
        """Prepare detector and backend for acquisition"""
        self.prepare_detector()
        self.prepare_data_backend()
        self.publish_file_location(done=False, successful=False)
        self.arm_acquisition()

    def prepare_detector(self) -> None:
        """Prepare detector for acquisition"""
        self.set_trigger(
            mapping_mode=MappingSource.MAPPING, trigger_source=TriggerSource.GATE, ignore_gate=0
        )
        self.parent.preset_real.put(self.parent.scaninfo.exp_time)
        self.parent.pixels_per_run.put(
            int(self.parent.scaninfo.num_points * self.parent.scaninfo.frames_per_trigger)
        )

    def prepare_data_backend(self) -> None:
        """Prepare data backend for acquisition"""
        self.parent.filepath.set(
            self.parent.filewriter.compile_full_filename(f"{self.parent.name}.h5")
        ).wait()
        file_path, file_name = os.path.split(self.parent.filepath.get())
        self.parent.hdf5.file_path.put(file_path)
        self.parent.hdf5.file_name.put(file_name)
        self.parent.hdf5.file_template.put("%s%s")
        self.parent.hdf5.num_capture.put(
            int(self.parent.scaninfo.num_points * self.parent.scaninfo.frames_per_trigger)
        )
        self.parent.hdf5.file_write_mode.put(2)
        # Reset spectrum counter in filewriter, used for indexing & identifying missing triggers
        self.parent.hdf5.array_counter.put(0)
        # Start file writing
        self.parent.hdf5.capture.put(1)

    def arm_acquisition(self) -> None:
        """Arm detector for acquisition"""
        self.parent.start_all.put(1)
        signal_conditions = [
            (
                lambda: self.parent.acquiring.read()[self.parent.acquiring.name]["value"],
                DetectorState.ACQUIRING,
            )
        ]
        if not self.wait_for_signals(
            signal_conditions=signal_conditions,
            timeout=self.parent.TIMEOUT_FOR_SIGNALS,
            check_stopped=True,
            all_signals=False,
        ):
            raise SitoroTimeoutError(
                f"Failed to arm the acquisition. Detector state {signal_conditions[0][0]}"
            )

    def on_unstage(self) -> None:
        """Unstage detector and backend"""
        pass

    def on_complete(self) -> None:
        """Complete detector and backend"""
        self.finished(timeout=self.parent.TIMEOUT_FOR_SIGNALS)
        self.publish_file_location(done=True, successful=True)

    def on_stop(self) -> None:
        """Stop detector and backend"""
        self.stop_detector()
        self.stop_detector_backend()

    def stop_detector(self) -> None:
        """Stops detector"""

        self.parent.stop_all.put(1)
        time.sleep(0.5)
        self.parent.erase_all.put(1)
        time.sleep(0.5)

        signal_conditions = [
            (
                lambda: self.parent.acquiring.read()[self.parent.acquiring.name]["value"],
                DetectorState.DONE,
            )
        ]

        if not self.wait_for_signals(
            signal_conditions=signal_conditions,
            timeout=self.parent.TIMEOUT_FOR_SIGNALS - self.parent.TIMEOUT_FOR_SIGNALS // 2,
            all_signals=False,
        ):
            # Retry stop detector and wait for remaining time
            raise SitoroTimeoutError(
                f"Failed to stop detector, timeout with state {signal_conditions[0][0]}"
            )

    def stop_detector_backend(self) -> None:
        """Stop the detector backend"""
        self.parent.hdf5.capture.put(0)

    def finished(self, timeout: int = 5) -> None:
        """Check if scan finished succesfully"""
        with self._lock:
            total_frames = int(
                self.parent.scaninfo.num_points * self.parent.scaninfo.frames_per_trigger
            )
            signal_conditions = [
                (self.parent.dxp.current_pixel.get, total_frames),
                (self.parent.hdf5.array_counter.get, total_frames),
            ]
            if not self.wait_for_signals(
                signal_conditions=signal_conditions,
                timeout=timeout,
                check_stopped=True,
                all_signals=True,
            ):
                logger.debug(
                    f"Sitoro missed a trigger: received trigger {self.parent.dxp.current_pixel.get()},"
                    f" send data {self.parent.hdf5.array_counter.get()} from total_frames"
                    f" {total_frames}"
                )
            self.stop_detector()
            self.stop_detector_backend()

    def set_trigger(
        self, mapping_mode: MappingSource, trigger_source: TriggerSource, ignore_gate: int = 0
    ) -> None:
        """
        Set triggering mode for detector

        Args:
            mapping_mode (MappingSource): Mapping mode for the detector
            trigger_source (TriggerSource): Trigger source for the detector, pixel_advance_signal
            ignore_gate (int): Ignore gate from TTL signal; defaults to 0

        """
        mapping = int(mapping_mode)
        trigger = trigger_source
        self.parent.collect_mode.put(mapping)
        self.parent.pixel_advance_mode.put(trigger)
        self.parent.ignore_gate.put(ignore_gate)


class SitoroPhoenix(PSIDetectorBase, SitoroEpicsDXPMultiElementSystem):
    # class SitoroPhoenix(PSIDetectorBase):
    """
    Sitoro Sitoro detector for Phoenix


    Parent class: PSIDetectorBase

    class attributes:
        custom_prepare_cls (SitoroSetup)        : Custom detector setup class,
                                                  inherits from CustomDetectorMixin

        PSIDetectorBase.set_min_readout (float) : Minimum readout time for the detector
        dxp1, .. dxpi, .. , dxpN (SitoroEpicsDXP)         : DXP parameters for Sitoro detector Nr i
        mca1, .. mcai, .. , mcaN (SitoroEpicsMCARecord)   : MCA parameters for Sitoro detector Nr i

        hdf5 (SitoroHDF5Plugins)                : HDF5 parameters for Sitoro detector
        MIN_READOUT (float)                     : Minimum readout time for the detector



    """

    # Specify which functions are revealed to the user in BEC client
    USER_ACCESS = ["describe"]

    # specify Setup class
    custom_prepare_cls = SitoroSetup
    # specify minimum readout time for detector
    MIN_READOUT = 3e-3
    TIMEOUT_FOR_SIGNALS = 1

    # specify class attributes

    # Parameters for individual detector elements
    # Note: need to wrote 'dxp: here, but not dxp'

    # dxp1 = Cpt(SitoroEpicsDXP, "dxp1:")
    # dxp2 = Cpt(SitoroEpicsDXP, "dxp2:")
    # dxp3 = Cpt(SitoroEpicsDXP, "dxp3:")
    # dxp4 = Cpt(SitoroEpicsDXP, "dxp4:")

    #
    # THIS IS NOT WELL-DONE as it take out one part of mca.py from ophy.py
    #
    #
    # mca1 = Cpt(SitoroEpicsMCARecordExtended, "mca1")
    # mca2 = Cpt(SitoroEpicsMCARecordExtended, "mca2")
    # mca3 = Cpt(SitoroEpicsMCARecordExtended, "mca3")
    # mca4 = Cpt(SitoroEpicsMCARecordExtended, "mca4")

    # need to write 'mca1', but not 'mca1:'
    # mca1 = Cpt(EpicsMCARecord, "mca1")
    # mca2 = Cpt(EpicsMCARecord, "mca2")
    # mca3 = Cpt(EpicsMCARecord, "mca3")
    # mca4 = Cpt(EpicsMCARecord, "mca4")

    #  other general parameters
    hdf5 = Cpt(SitoroHDF5Plugins, "HDF1:")

    # stop_all = Cpt(EpicsSignal, "StopAll")
    # erase_all = Cpt(EpicsSignal, "EraseAll")
    # start_all = Cpt(EpicsSignal, "StartAll")
    # state = Cpt(EpicsSignal, "Acquiring")  # <-- This is from cSAX implementation
    # acquiring = Cpt(EpicsSignal, "Acquiring")
    # preset_mode = Cpt(EpicsSignal, "PresetMode")  # 0 No preset 1 Real time 2 Events 3 Triggers
    # preset_real = Cpt(EpicsSignal, "PresetReal")
    # preset_events = Cpt(EpicsSignal, "PresetEvents")
    # preset_triggers = Cpt(EpicsSignal, "PresetTriggers")

    # _________________ General Epic parameters

    # changes Oct 2024
    # triggers--> max_triggers,
    # events-->max_events
    # input_count_rate--> max_input_count_rate
    # output_count_rate--> max_output_count_rate

    # max_triggers = Cpt(EpicsSignalRO, "MaxTriggers", lazy=True)
    # max_events = Cpt(EpicsSignalRO, "MaxEvents", lazy=True)

    # max_input_count_rate = Cpt(EpicsSignalRO, "MaxInputCountRate", lazy=True)
    # max_output_count_rate = Cpt(EpicsSignalRO, "MaxOutputCountRate", lazy=True)

    # collect_mode = Cpt(EpicsSignal, "CollectMode")  # 0 MCA spectra, 1 MCA mapping
    # pixel_advance_mode = Cpt(EpicsSignal, "PixelAdvanceMode")
    # ignore_gate = Cpt(EpicsSignal, "IgnoreGate")
    # input_logic_polarity = Cpt(EpicsSignal, "InputLogicPolarity")
    # auto_pixels_per_buffer = Cpt(EpicsSignal, "AutoPixelsPerBuffer")
    # pixels_per_buffer = Cpt(EpicsSignal, "PixelsPerBuffer")
    # pixels_per_run = Cpt(EpicsSignal, "PixelsPerRun")
    # print(pixel_per_run
    # if "SITORO" in prefix:
    nd_array_mode = Cpt(EpicsSignal, "NDArrayMode")
    # endif


if __name__ == "__main__":
    sitoro = SitoroPhoenix(name="sitoro", prefix="X07MB-SITORO:", sim_mode=True)
