"""
Module for the PhoenixTrigger class to connect to the ADC card
that creates TTL signals to trigger cameras and detectors at Phoenix.

TO Do

-- allow for variable dwell times
-- add erase/Start for XMAP and FALCON
-- check values for time.sleep()
-- check in on_triggerthe status check for Falcon
-- rework togther with Xiaoquiang the functionality of involved EPICS channels
   (Callbacks etc.) to minimize the need of sleeping times.

"""

import enum
import time

import numpy as np
from bec_lib import bec_logger
from ophyd import Component as Cpt
from ophyd import DeviceStatus, EpicsSignal, EpicsSignalRO, Kind
from ophyd_devices.interfaces.base_classes.psi_detector_base import (
    CustomDetectorMixin,
    PSIDetectorBase,
)

logger = bec_logger.logger

DETECTOR_TIMEOUT = 5


class PhoenixTriggerError(Exception):
    """PhoenixTrigger specific error"""


class SAMPLING(int, enum.Enum):
    """Sampling Done PV

    This class serves redabilty of missinx class and ensure correct setting of
    certain conditions.
    defiend preset values for certain states to be called in the
    mixing class, where we for example check whether the sampling is done of not
    by comparing with  SAMPLING.DONE
    xxx==SAMPLING.DONE

    """

    RUNNING = 0
    DONE = 1


class PhoenixTriggerSetup(CustomDetectorMixin):
    """
    Mixin Class to setup the PhoenixTrigger device

    """

    def on_stage(self) -> None:
        """

        On stage actions which are executed upon staging the device


        """
        if self.parent.scaninfo.scan_type == "step":  # check whether we use step scanning
            ###############
            # next lines ensure that TTL trigger is on single sampling mode (posible )
            ##############
            self.parent.start_csmpl.set(0)
            time.sleep(0.1)
            self.parent.total_cycles.set(1)
            self.parent.smpl.put(1)
            time.sleep(0.5)
            #####
            #  set sampling to dwell time of scan
            ######
            self.parent.total_cycles.set(np.ceil(self.parent.scaninfo.exp_time * 5))

            logger.info(f"Device {self.parent.name} was staged for step scan")

    def on_unstage(self) -> None:
        """On unstage actions which are executed upon unstaging the device"""

        self.on_stop()

    def on_trigger(self) -> DeviceStatus:
        """On trigger actions which are executed upon triggering the device"""

        # TODO Test the proper check for the falcon state
        # Check first that falcon is set to acquiring
        falcon = self.parent.device_manager.devices.get("falcon_nohdf5", None)  # get device
        timeout = 1
        if falcon is not None:
            # TODO Check that falcon.state.get() == 1 is the correct check.
            # --> When is the falcon acquiring, this assumes 1?
            # self.wait_for_signals is defined in PSI_detector_base.CustomDetectorMixin
            #

            if not self.wait_for_signals([(falcon.state.get, 1)], timeout=timeout):
                raise PhoenixTriggerError(
                    f"Device {self.parent.name} is not ready to take trigger, timeout due to waiting for Falcon to get ready. Timeout after {timeout}s"
                )

        if self.parent.scaninfo.scan_type == "step":
            time.sleep(0.2)
            self.parent.smpl.put(1)
            # Minimum of 1 cycle has to be waited. Cycle == 0.2s

            time.sleep(0.2)

            # Trigger function from ophyd.Device returns a DeviceStatus. This function
            # starts a process that creates a DeviceStatus, and waits for the signal_conditions
            # self.parent.smpl_done.get to change to the value SAMPLING.DONE
            # Once this takes place, the DeviceStatus.done flag will be set to True.
            # When BEC calls trigger() on the devices, this method will be called assuming that
            # the devices config softwareTrigger=True is set.
            # In ScanBase, the _at_each_point function calls
            # self.stubs.wait(wait_type="trigger", group="trigger", wait_time=self.exp_time)
            # which ensures that the DeviceStatus object resolves before continuing,
            # i.e. DeviceStatus.done = True

            status = self.wait_with_status(
                signal_conditions=[(self.parent.smpl_done.get, SAMPLING.DONE)],
                timeout=5 * self.parent.scaninfo.exp_time,  # Check if timeout is appropriate
                check_stopped=True,
            )

            # explanation of last line  (self.parent.smpl_done.get, SAMPLINGDONE.DONE)
            # creates a tuple which defines a
            # condition, which is tested in self.wait_with_status, here it tests for :
            # (self.parent.smpl_done.get() ==  SAMPLINGDONE.DONE),
            # where SAMPLINGDONE.DONE =1, as set in code above
            # As this is in mixing class (PhoenixtriggerSetup), parent.sample_done is defined in
            # main class as smpl_done = Cpt(EpicsSignalRO, "SMPL-DONE", kind=Kind.config)

            return status  # should this be in if clause level or outside?
        # endif

    def on_stop(self) -> None:
        """
        Actions to stop the Device
        Here the device is switched back to continuous sampling.

        """

        self.parent.total_cycles.set(5)

        self.parent.start_csmpl.set(1)
        time.sleep(0.5)
        self.parent.smpl.put(1)
        time.sleep(0.5)
        self.parent.smpl.put(1)
        time.sleep(0.2)
        if self.parent.smpl_done.get() == SAMPLING.RUNNING:
            return
        self.parent.smpl.put(1)


class PhoenixTrigger(PSIDetectorBase):
    """
    Class for PHOENIX TTL hardware trigger: 'X07MB-OP2:'

    This device is used to trigger communicate with an ADC card
    that creates TTL signals to trigger cameras and detectors at Phoenix.

    """

    ##################################################################
    #
    # The Variable USER_ACCESS contains an ascii list of functions which will be
    # visible in dev.TTL. Here, this list is empty.
    # note that components are alway public
    #
    ##################################################################

    USER_ACCESS = []

    ####################################################################
    #
    # # specify Setup class into variable custom_prepare_cls
    #
    ####################################################################

    custom_prepare_cls = PhoenixTriggerSetup

    ###################################################################
    # in __init__ of PSIDetectorBase will the initialzed by
    # self.custom_prepare = self.custom_prepare_cls(parent=self, **kwargs)
    # making the instance of PSIDetectorBase availble to functions
    # in PhoenixTriggerSetup.
    #
    # This inherits, among other things, the input parameters, such as
    # the notable prefix, which is here 'X07MB-OP2:'
    #
    # The input of Component=Cpt is Cpt(deviceClass,suffix)
    # if Cpt is used in a class, which has interited Device, here via:
    #
    #  (Here PhoenixTrigger  <--  PSIDetectorBase  <-  Device
    #
    # then Cpt will construct - magically- the
    # Epics channel name  =  prefix+suffix,
    #
    # for example
    # 'X07MB-OP2:'  + 'START-CSMPL' -> 'X07MB-OP2:'  + 'START-CSMPL'
    #
    # to construct names, for now we keep the convention to derive
    # them from the EPICS NAMES (such as ) X07MB-OP2:SMPL-DONE  --> smpl_done
    #
    # this mean access to channel using dev.PH_TTL.smpl_done.get()
    #
    #
    ###################################################################

    start_csmpl = Cpt(
        EpicsSignal, "START-CSMPL", kind=Kind.config, put_complete=True
    )  # cont on / off
    intr_count = Cpt(
        EpicsSignal, "INTR-COUNT", kind=Kind.config, put_complete=True
    )  # conter run up
    total_cycles = Cpt(
        EpicsSignal, "TOTAL-CYCLES", kind=Kind.config, put_complete=True
    )  # cycles set
    smpl = Cpt(
        EpicsSignal, "SMPL", kind=Kind.config, put_complete=True
    )  # start sampling --> aquire
    smpl_done = Cpt(
        EpicsSignalRO, "SMPL-DONE", kind=Kind.config
    )  # show trigger is done, consider using string=True


if __name__ == "__main__":

    # Test the PhoenixTrigger class

    trigger = PhoenixTrigger(name="trigger", prefix="X07MB-OP2:")
    trigger.wait_for_connection(all_signals=True)
    trigger.read()
    trigger.read_configuration()

    trigger.stage()
    device_status = trigger.trigger()
    device_status.wait()
    trigger.unstage()
