"""

SCAN PLUGINS for PHOENIX

All new scans should be derived from ScanBase. ScanBase provides various methods that can be customized and overriden
but they are executed in a specific order:

- self.initialize                        # initialize the class if needed
- self.read_scan_motors                  # used to retrieve the start position (and the relative position shift if needed)
- self.prepare_positions                 # prepare the positions for the scan. The preparation is split into multiple sub fuctions:
    - self._calculate_positions          # calculate the positions
    - self._set_positions_offset         # apply the previously retrieved scan position shift (if needed)
    - self._check_limits                 # tests to ensure the limits won't be reached
- self.open_scan                         # send an open_scan message including the scan name, the number of points and the scan motor names
- self.stage                             # stage all devices for the upcoming acquisiton
- self.run_baseline_readings            # read all devices to get a baseline for the upcoming scan
- self.pre_scan                           # perform additional actions before the scan starts
- self.scan_core                         # run a loop over all position
    - self._at_each_point(ind, pos)      # called at each position with the current index and the target positions as arguments
- self.finalize                          # clean up the scan, e.g. move back to the start position; wait everything to finish
- self.unstage                           # unstage all devices that have been staged before
- self.cleanup                           # send a close scan message and perform additional cleanups if needed
"""

# imports in ScanBase
# from __future__ import annotations

# import ast
# import enum
# import threading
# import time
# import uuid
# from abc import ABC, abstractmethod
# from typing import Any, Literal

# import numpy as np

# from bec_lib.device import DeviceBase
# from bec_lib.devicemanager import DeviceManagerBase
# from bec_lib.endpoints import MessageEndpoints
# from bec_lib.logger import bec_logger

# from .errors import LimitError, ScanAbortion
# from .path_optimization import PathOptimizerMixin
# from .scan_stubs import ScanStubs
# end  imports in ScanBase

# import time

# import numpy as np

import time

import numpy as np

# from bec_lib.endpoints import MessageEndpoints
from bec_lib.logger import bec_logger
from bec_server.scan_server.scans import ScanArgType, ScanBase

from phoenix_bec.scripts.phoenix import PhoenixBL

# from bec_lib import messages
# from bec_server.scan_server.errors import ScanAbortion
# from bec_server.scan_server.scans import FlyScanBase, RequestBase, ScanArgType, ScanBase

# logger = bec_logger.logger


logger = bec_logger.logger


class LogTime:

    def __init__(self):
        logger.success("init LogTime")
        self.t0 = time.time()

    def p_s(self, x):
        now = time.time()
        # delta=now-self.t0
        m = str(now) + " sec " + x
        logger.success(m)
        # making the instance of PSID
        # self.t0=now
        file = open("MyLogfile.txt", "a")
        file.write(m + "\n")
        file.close


class PhoenixScanBaseTTL(ScanBase):
    """
    Base scan cl p_s('init scrips.phoenix.scans.PhoenixLineScan')
    """

    def scan_core(self):
        """perform the scan core procedure"""
        self.p_s("PhoenixScanBaseTT.scan_core")
        for ind, pos in self._get_position():
            for self.burst_index in range(self.burst_at_each_point):
                self.p_s("PhoenixScanBaseTT.scan_core in loop ")

                yield from self._at_each_point(ind, pos)
            self.burst_index = 0

    def _at_each_point(self, ind=None, pos=None):
        self.p_s("PhoenixScanBaseTT._at_each_point")
        yield from self._move_scan_motors_and_wait(pos)
        time.sleep(self.settling_time)
        yield from self.stubs.trigger(min_wait=self.exp_time)
        yield from self.stubs.read(group="monitored", point_id=self.point_id)

        self.point_id += 1
        self.p_s("done")


class PhoenixLineScan(PhoenixScanBaseTTL):

    scan_name = "phoenix_line_scan"
    required_kwargs = ["steps", "relative"]
    arg_input = {
        "device": ScanArgType.DEVICE,
        "start": ScanArgType.FLOAT,
        "stop": ScanArgType.FLOAT,
    }
    arg_bundle_size = {"bundle": len(arg_input), "min": 1, "max": None}
    gui_config = {
        "Movement Parameters": ["steps", "relative"],
        "Acquisition Parameters": ["exp_time", "burst_at_each_point"],
    }

    def __init__(
        self,
        *args,
        exp_time: float = 0,
        steps: int = None,
        relative: bool = False,
        burst_at_each_point: int = 1,
        setup_device: str = None,
        **kwargs,
    ):
        """
                A phoenix line scan for one or more motors.

                Args:
                    *args (Device, float, float): pairs of device / start position / end position
                    exp_time (float): exposure time in s. Default: 0
                    steps (int): number of steps. Default: 10
                    relative (bool): if True, the start and end positions are relative to the current position. Default: False
                    burst_a 	Specifies the level of type checking analysis to perform.
        ans.line_scan(dev.motor1, -5, 5, dev.motor2, -5, 5, steps=10, exp_time=0.1, relative=True)

        """
        # from phoenix_bec.scripts.phoenix import PhoenixBL
        self.p_s = PhoenixBL.my_log

        self.p_s("init scripts.phoenix.scans.PhoenixLineScan")

        super().__init__(
            exp_time=exp_time, relative=relative, burst_at_each_point=burst_at_each_point, **kwargs
        )
        self.steps = steps
        self.setup_device = setup_device

        time.sleep(1)
        self.p_s("done")

    def _calculate_positions(self) -> None:
        self.p_s("PhoenixLineScan._calculate_positions")
        axis = []
        for _, val in self.caller_args.items():
            ax_pos = np.linspace(val[0], val[1], self.steps, dtype=float)
            axis.append(ax_pos)
        self.positions = np.array(list(zip(*axis)), dtype=float)
        self.p_s("done")
