#from unittest import mock
import numpy as np
#import pandas
#import pytest
#from bec_lib import messages
#import device_server
#from ophyd import Component as Cpt
from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO
#from ophyd import FormattedComponent as FCpt
#from ophyd import Kind, PVPositioner, Signal
#from ophyd.flyers import FlyerInterface
#from ophyd.pv_positioner import PVPositionerComparator
#from ophyd.status import DeviceStatus, SubscriptionStatus

import time as tt

#import ophyd
import os
import sys

#logger = bec_logger.logger
# load simulation

#bec.config.load_demo_config()

bec.config.update_session_with_file("config/config_1.yaml")

os.system('mv *.yaml tmp')



class PhoenixBL:

   #define some epics channels

   def __init__(self):
      from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO
      from ophyd import Component as Cpt
      self.ScanX = EpicsMotor(name='ScanX',prefix='X07MB-ES-MA1:ScanX')
      self.ScanY = EpicsMotor(name='ScanY',prefix='X07MB-ES-MA1:ScanY')
      self.DIODE = EpicsSignal(name='SI',read_pv='X07MB-OP2-SAI_07:MEAN')
      self.SIG   = Cpt(EpicsSignal,name='we',read_pv="X07MB-OP2-SAI_07:MEAN")
      self.SMPL   = EpicsSignal(name='SMPL',read_pv='X07MB-OP2:SMPL')
      self.CYCLES = EpicsSignal(name='SMPL',read_pv='X07MB-OP2:TOTAL-CYCLES',write_pv='X07MB-OP2:TOTAL-CYCLES')
      self.fielda  =EpicsSignal(name='SMPL',read_pv='X07MB-SCAN:scan1.P1SP',write_pv='X07MB-SCAN:scan1.P1SP')
#end class

ph=PhoenixBL()

print('---------------------------------')

# scan will not diode
print(' SCAN DO NOT READ DIODE ')
dev.PH_curr_conf.readout_priority='baseline'  # do not read detector
ti=tt.time_ns()
s1=scans.line_scan(dev.PH_ScanX_conf,0,0.002,steps=4,exp_time=.01,relative=False,delay=2)
tf=tt.time_ns()

print('elapsed time',(tf-ti)/1e9)
# scan will read diode
print(' SCAN READ DIODE ')
tt.sleep(2)
dev.PH_curr_conf.readout_priority='monitored' # read detector

s2=scans.line_scan(dev.PH_ScanX_conf,0,0.002,steps=11,exp_time=.3,relative=False,delay=2)


"""
next lines do not work as pandas is not installed on test system

res1 = s1.scan.to_pandas()
re1  = res1.to_numpy()
print('Scana')
print(res1)
print('')
print('Scan2 at pandas ')
print(res2)
print('Scan2 as numpy ')
print(res2)
"""





