#from unittest import mock                                                                                                                                              
import numpy as np    
#import pandas                                                                                                                                                 
#import pytest                                                                                                                                                          
#from bec_lib import messages                                                                                                                                           
#import device_server                                                                                                                                                   
#from ophyd import Component as Cpt                                                                                                                                     
from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO                                                                                                       
#from ophyd import FormattedComponent as FCpt                                                                                                                           
#from ophyd import Kind, PVPositioner, Signal                                                                                                                           
#from ophyd.flyers import FlyerInterface                                                                                                                                
#from ophyd.pv_positioner import PVPositionerComparator                                                                                                                 
#from ophyd.status import DeviceStatus, SubscriptionStatus                                                                                                              
from bec_lib.logger import bec_logger
logger = bec_logger.logger

import time as tt                                                                                                                                                      
                                                                                                                                                                       
#import ophyd                                                                                                                                                          
import os                                                                                                                                                              
import sys                                                                                                                                                             
                                                                                                                                                                       
#logger = bec_logger.logger                                                                                                                                            
# load simulation                                                                                                                                                                                                                                                                                           
#bec.config.load_demo_config()                                                                                                                                        

# .. define base path for directory with scripts 
                                                                                                                                                                              

class PhoenixBL(): 
   """
   
   General class for PHOENIX beamline 

   """                                                                                                                                                                    
   #define some epics channels                                                                                                                                         
   #scan_name = "phoenix_base"    

   def __init__(self): 
      """
      init  PhoenixBL() in ConfigPHOENIX.config.phoenix 
      """
      import os                                                                                                                                                 
      #from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO 
      #from ophyd import Component as Cpt                                                                                                                                                                     
      #self.ScanX = EpicsMotor(name='ScanX',prefix='X07MB-ES-MA1:ScanX')                                                                                                
      #self.ScanY = EpicsMotor(name='ScanY',prefix='X07MB-ES-MA1:ScanY')                                                                                                
      #self.DIODE = EpicsSignal(name='SI',read_pv='X07MB-OP2-SAI_07:MEAN')                                                                                              
      #self.SIG   = Cpt(EpicsSignal,name='we',read_pv="X07MB-OP2-SAI_07:MEAN")                                                                                          
      #self.SMPL   = EpicsSignal(name='SMPL',read_pv='X07MB-OP2:SMPL')                                                                                                  
      #self.CYCLES = EpicsSignal(name='SMPL',read_pv='X07MB-OP2:TOTAL-CYCLES',write_pv='X07MB-OP2:TOTAL-CYCLES')                                                        

      # load local configuration 

      print('init PhoenixBL')
      
      self.path_scripts_local = '/data/test/x07mb-test-bec/bec_deployment/LocalScripts/'
      self.path_config_local  =  self.path_scripts_local  + 'ConfigPHOENIX/'   # base dir for local configurations
      self.path_devices_local =  self.path_config_local   + 'device_config/'   # local yamal file
      self.file_device_conf   =  self.path_devices_local  + 'phoenix_devices.yaml'

      #bec.config.update_session_with_file(self.file_device_conf)
      # last command created yaml backup, for now just move it away 
      #os.system('mv *.yaml '+Devices_local+'/recovery_configs') 
      #os.system('mv *.yaml tmp')   
   
   def read_def_config():
      bec.config.update_session_with_file(self.file_device_conf)

         
   def print_setup(self):
      """
      docstring print_setup 


      """
   
      print(self.path_scripts_local)

