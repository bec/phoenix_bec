import epics as ep

################
#  Testing base class epics
#  The raw code is located in the file
#  bec_client_venv/lib64/python3.11/site-packages/epics/__init__.py
# in bec start script by run -i
# option -i ensures taht iphyjon shell and scritp are in same namespace
# run -i BaseClass_Epics.py


pvname='X07MB-OP2-SAI_07:INP-OFS'

print('ep.cainfo(pvname)')
ep.cainfo(pvname)
print('caget(pvname)')

ep.caput(pvname,0.5,wait=.1)
res=ep.caget(pvname)
print('1:',res)
ep.caput(pvname,0.01,wait=.1)
res=ep.caget(pvname)
print('2',res)
print('Start camon to see effect , change value of pv ')
ep.camonitor(pvname)
