from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO
from ophyd import Component as Cpt

# option I via direct acces to classes


def print_dic(clname, cl):
    print("")
    print("-------- ", clname)
    for ii in cl.__dict__:
        if "_" not in ii:

            try:
                print(ii, " ----  ", cl.__getattribute__(ii))
            except:
                print(ii)


ScanX = EpicsMotor(name="ScanX", prefix="X07MB-ES-MA1:ScanX")
ScanY = EpicsMotor(name="ScanY", prefix="X07MB-ES-MA1:ScanY")
DIODE = EpicsSignal(name="SI", read_pv="X07MB-OP2-SAI_07:MEAN")
SMPL = EpicsSignal(name="SMPL", read_pv="X07MB-OP2:SMPL")
CYCLES = EpicsSignal(
    name="SMPL", read_pv="X07MB-OP2:TOTAL-CYCLES", write_pv="X07MB-OP2:TOTAL-CYCLES"
)


# prefix='XXXX:'
y_cpt = Cpt(EpicsMotor, "ScanX")
# Option 2 using component

device_ins = Device("X07MB-ES-MA1:", name=("device_name"))
print(" initialzation of  device_in=Device(X07MB-ES-MA1:,name=(device_name)")
print("device_ins.__init__")
print(device_ins.__init__)
print_dic("class Device", Device)
print_dic("instance of device device_ins", device_ins)


print(" ")
print("DEFINE class StageXY... prefix variable not defined ")


class StageXY(Device):
    # Here the whole namespace and finctionality
    # of Device(Blueskyinterface,Pphydobject) is inherited
    # into class StageXY using Device
    # device requires as input parameters the prefix
    # in the initialization of Cpt there is some magic
    # hard to understand, moist likely using calss methods..
    #

    x = Cpt(EpicsMotor, "ScanX")
    y = Cpt(EpicsMotor, "ScanY")


# end class


print()
print("init xy_stage, use input parameter from Device and prefix is defined in call ")
xy_stage = StageXY("X07MB-ES-MA1:", name="stageXXX")

print_dic("class StageXY", StageXY)
print_dic("instance of StageXY", xy_stage)


#############################################
# This is basic bluesky
# Epics motor def seems to use init params in device, whcih are
# __init__(
#        self,
#        prefix="",
#        *,
#        name,
#        kind=None,
#        read_attrs=None,
#        configuration_attrs=None,
#        parent=None,
#        **kwargs,
#    ):
#
#########################################################

print("xy_stage.x.prefix")
print(xy_stage.x.prefix)
xy_stage.__dict__


# to move motor use
# stage.x.move(0)
# to see all dict
# stage.x.__dict__
