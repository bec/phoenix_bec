from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO
from ophyd import Component as Cpt


from phoenix_bec.local_scripts.Examples.my_ophyd import Device,EpicsMotor, EpicsSignal, EpicsSignalRO
from  phoenix_bec.local_scripts.Examples.my_ophyd import Component as Cpt

############################################
#
# KEEP my_ophyd zipped to avoid chaos local version does not run
# so this is rather useless
#
##########################################



#option I via direct acces to classes

def print_dic(clname,cl):
    print('')
    print('-------- ',clname)
    for ii in cl.__dict__:
        if '_' not in ii:

            try:
                print(ii,' ----  ',cl.__getattribute__(ii))
            except:
                print(ii)




ScanX = EpicsMotor(name='ScanX',prefix='X07MB-ES-MA1:ScanX')
ScanY = EpicsMotor(name='ScanY',prefix='X07MB-ES-MA1:ScanY')
DIODE = EpicsSignal(name='SI',read_pv='X07MB-OP2-SAI_07:MEAN')
SMPL   = EpicsSignal(name='SMPL',read_pv='X07MB-OP2:SMPL')
CYCLES = EpicsSignal(name='SMPL',read_pv='X07MB-OP2:TOTAL-CYCLES',write_pv='X07MB-OP2:TOTAL-CYCLES')


#prefix='XXXX:'
y_cpt = Cpt(EpicsMotor, 'ScanX')
# Option 2 using component

device_ins=Device('X07MB-ES-MA1:',name=('device_name'))
print(' initialzation of  device_in=Device(X07MB-ES-MA1:,name=(device_name)')
print('device_ins.__init__')
print(device_ins.__init__)
print_dic('class Device',Device)
print_dic('instance of device device_ins',device_ins)


print(' ')
print('DEFINE class StageXY... prefix variable not defined ')

EpicsMotor, 'ScanY'
"""
class MyCpt(typing.Generic[K]):

    def __init__(
        self,
        cls: Type[K],
        suffix: Optional[str] = None,
        *,
        lazy: Optional[bool] = None,
        trigger_value: Optional[Any] = None,
        add_prefix: Optional[Sequence[str]] = None,
        doc: Optional[str] = None,
        kind: Union[str, Kind] = Kind.normal,
        **kwargs,
    ):
        self.attr = None  # attr is set later by the device when known
        self.cls = cls
        self.kwargs = kwargs
        #self.lazy = lazy if lazy is not None else self.lazy_default
        self.suffix = suffix
        self.doc = doc
        self.trigger_value = trigger_value  # TODO discuss
        self.kind = Kind[kind.lower()] if isinstance(kind, str) else Kind(kind)
        if add_prefix is None:
            add_prefix = ("suffix", "write_pv")
        self.add_prefix = tuple(add_prefix)
        self._subscriptions = collections.defaultdict(list)
        print(' ')
"""


in Device we have this class method
Class device(..):
....
  @classmethod
    def _initialize_device(cls):
....


class StageXY(Device):
    # Here the whole namespace and finctionality
    # of Device(Blueskyinterface,Pphydobject) is inherited
    # into class StageXY

    x = Cpt(EpicsMotor, 'ScanX')
    y = Cpt(EpicsMotor, 'ScanY')

# end class




print()
print('init xy_stage, use input parameter from Device and prefix is defined in call ')
xy = StageXY('X07MB-ES-MA1:', name='xy_name')

print_dic('class StageXY',StageXY)
print_dic('instance of StageXY',xy)




#print('xy.x.prefix')
#print(xy.x.prefix)
xy.__dict__
