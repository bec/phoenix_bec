"""
Scritpt to be developed as template for phoenic scritps
"""

# from unittest import mock
import numpy as np

# import pandas
# import pytest
# from bec_lib import messages
# import device_server
# from ophyd importPhoenixTemplate.pyitioner import PVPositionerComparator
# from ophyd.status import DeviceStatus, SubscriptionStatus

import time

# import ophyd
import os
import sys
import importlib
import ophyd


#
phoenix.add_phoenix_config()
# bec.config.update_session_with_file('./ConfigPHOENIX/device_config/phoenix_devices.yaml')

time.sleep(1)

s1 = scans.line_scan(dev.ScanX, 0, 0.1, steps=4, exp_time=0.2, relative=False, delay=2)

s2 = scans.phoenix_line_scan(dev.ScanX, 0, 0.1, steps=4, exp_time=0.2, relative=False, delay=2)

res1 = s1.scan.to_pandas()
re1 = res1.to_numpy()
w1 = PH.PhGroup("Bec Linescan")
w1.linescan2group(s1)

print("res1")
print(res1)
print("as numpy")
print("re1")


res2 = s2.scan.to_pandas()
re2 = res2.to_numpy()
w2 = PH.PhGroup("PHOENIX Linescan")
w2.linescan2group(s2)

print("res2")
print(res2)
print("as numpy")
print("re2")


print(s1)
print("---------------------------------")

"""
# scan will not diode
print(' SCAN DO NOT READ DIODE ')
dev.PH_curr_conf.readout_priority='baseline'  # do not read detector
ti=tt.time_ns()
s1=scans.line_scan(dev.PH_ScanX_conf,0,0.002,steps=4,exp_time=.01,relative=False,delay=2)
tf=tt.time_ns()

print('elapsed time',(tf-ti)/1e9)
# scan will read diode
print(' SCAN READ DIODE ')s is not installed on test system
ScanX_conf,0,0.002,steps=11,exp_time=.3,relative=False,delay=2)


#next lines do not work as pandas is not installed on test system

res1 = s1.scan.to_pandas()
re1  = res1.to_numpy()
print('Scana')
print(res1)
print('')
print('Scan2 at pandas ')
print(res2)
print('Scan2 as numpy ')
print(res2)

"""
