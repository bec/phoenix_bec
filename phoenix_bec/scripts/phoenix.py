# from unittest import mock
import os
import sys
import time

import numpy as np

# import pandas
# import pytest
# from bec_lib import messages
# import device_server
# from ophyd import Component as Cpt
from ophyd import Device, EpicsMotor, EpicsSignal, EpicsSignalRO

# from ophyd import FormattedComponent as FCpt
# from ophyd import Kind, PVPositioner, Signal
# from ophyd.flyers import FlyerInterface
# from ophyd.pv_positioner import PVPositionerComparator
# from ophyd.status import DeviceStatus, SubscriptionStatus

from bec_lib.config_helper import ConfigHelper
from bec_lib.logger import bec_logger

logger = bec_logger.logger


# import ophyd
# logger = bec_logger.logger
# load simulation
# bec.config.load_demo_config()

# .. define base path for directory with scripts


class PhoenixBL:
    """
    #
    #    General class for PHOENIX beamline located in phoenix_bec/phoenic_bec/scripts
    #
    """

    t0 = time.time()

    def __init__(self):
        """
        init  PhoenixBL() in phoenix_bec/scripts

        """
        import os

        print("..... init PhoenixBL from phoenix_bec/scripts/phoenix.py")

        # load local configuration

        self.path_scripts_local = (
            "/data/test/x07mb-test-bec/bec_deployment/phoenix_bec/phoenix_bec/local_scripts/"
        )

        self.path_config_local = (
            self.path_scripts_local + "TEST_ConfigPhoenix/"
        )  # base dir for local configurations

        self.path_devices_local = (
            self.path_config_local + "Local_device_config/"
        )  # local yamal file

        self.file_devices_file_local = self.path_devices_local + "phoenix_devices.yaml"

        self.path_phoenix_bec = "/data/test/x07mb-test-bec/bec_deployment/phoenix_bec/"
        self.path_devices = self.path_phoenix_bec + "phoenix_bec/device_configs/"
        # yamal file for default configuration

        self.file_devices_file = (
            self.path_phoenix_bec + "phoenix_bec/device_configs/phoenix_devices.yaml"
        )  # local yamal file

        self.file_devices_tmp = (
            self.path_phoenix_bec + "phoenix_bec/device_configs/current_devices_tmp.yaml"
        )  # tmp configuration file. Will be electronicall created and appended if needed

        self.t0 = time.time()

    def read_local_phoenix_config(self):
        print("read file ")
        print(self.file_phoenix_devices_file)
        bec.config.update_session_with_file(self.file_devices_file_local)

    def create_base_config(self):
        # create a yaml file from standard configuration
        os.system("cat " + self.file_devices_file + " > " + self.file_devices_tmp)
        # os.system("ls -altr" + self.path_phoenix_bec + "phoenix_bec/devices")
        bec.config.update_session_with_file(self.file_devices_tmp)

    def add_phoenix_config(self):
        print("add_phoenix_config ")
        print("self.file_devices_file")
        bec.config.update_session_with_file(self.tmp.file_devices_file)

    def add_xmap(self):
        print("add xmap ")

        os.system("cat " + self.path_devices + "phoenix_xmap.yaml" + " >> " + self.file_devices_tmp)
        bec.config.update_session_with_file(self.file_devices_tmp)

    def add_falcon(self):
        print("add_falcon to existing configuration ")

        os.system(
            "cat " + self.path_devices + "phoenix_falcon.yaml" + " >> " + self.file_devices_tmp
        )
        bec.config.update_session_with_file(self.file_devices_tmp)

    def load_falcon(self):

        print("load_falcon")
        bec.config.update_session_with_file(self.path_devices + "phoenix_falcon.yaml")

    def show_phoenix_setup(self):
        print(self.path_phoenix_bec)
        os.system("cat " + self.path_phoenix_bec + "phoenix_bec/scripts/Current_setup.txt")

    @classmethod
    def my_log(cls, x):
        """
        class method allows to write a user defined log file
        time is seconds relative to some point max 10 minutes ago

        """

        print(time.time())
        now = time.time() - (86400 * (time.time() // 86400))
        now = now - 3600.0 * (now // 3600.0)
        now = now - 600.0 * (now // 600.0)
        m = str(now) + " sec " + x

        logger.success(m)

        file = open("MyLogfile.txt", "a")
        file.write(m + "\n")
        file.close


class PhGroup:
    """
        Class to create data groups
        compatible with larch groups

        initialize by

        ww=PhGroup('YourLabel')

        it creates a group
        with default attributes

        ww.label = 'YourLabel' --- for compatibility with larch groups
        ww.description =YourLabel'

        Further data can be added with new tags by

        ww.newtag=67
    (bec_venv) [gac-x07mb@x07mb-bec-001 phoenix_bec]$

        ww.keys()         -- list all keys
        ww.linescan2group -- converts bec linescan data to group format

    """

    def __init__(self, description):

        setattr(self, "description", description)
        # atribute 'label' for compatibility woith La groups...
        setattr(self, "label", description)
        # if type(NameTag)==list:
        #    for i in NameTag:
        #        setattr(self,i,None)
        #    #endfor
        # else:
        #    setattr(self,NameTag,None)
        # endif

    def add(self, NameTag, content):
        """
        Add tags to group...

        Parameters
        ----------
        NameTag : TYPE
            DESCRIPTION.
        content : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """

        setattr(self, NameTag, content)

    def keys(self):
        """
        Method gets all atributes, which are not methods
        and which do not start with __


        Returns
        -------
        box : TYPE
            DESCRIPTION.

        """
        box = []

        for i in self.__dir__():
            if "__" not in i:
                # print(i)
                if str(type(self.__getattribute__(i))) != "<class 'method'>":
                    box.append(i)
            # endif
        # endfor
        return box

    def linescan2group(self, this_scan):
        """

        method merges results of linescan into group and
        creates for each data a numpy variable constructed as

        group_name.{device_name}_{variable_name}_val   (for value )
        group_name.{device_name}_{variable_name}_ts    (for timestamp )


        """

        print("keys")
        print(this_scan.scan.data.keys())
        for outer_key in this_scan.scan.data.keys():
            print("outer_key", outer_key)
            n_outer = len(this_scan.scan.data.keys())
            for inner_key in this_scan.scan.data[outer_key].keys():
                print("inner_key", inner_key)
                # calculate nunber of points
                n_inner = len(this_scan.scan.data[outer_key][inner_key].keys())
                value = np.zeros(n_inner)
                timestamp = np.zeros(n_inner)
                for i in range(n_inner):
                    try:
                        value[i] = this_scan.scan.data[outer_key][inner_key][i]["value"]
                    except:
                        value = None
                    try:
                        timestamp[i] = this_scan.scan.data[outer_key][inner_key][i]["timestamp"]
                    except:
                        timestamp[i] = None
                    # endfor
                    self.add(inner_key + "_" + outer_key + "_val", value)
                    self.add(inner_key + "_" + outer_key + "_ts", timestamp)
                # endfor
            # endfor
        # endfor

    # enddef
